#include <stdio.h>
#include <string.h>
#include <math.h>

#define DSIN sin
#define DCOS cos
#define DATAN atan
#define DLOG log
#define DEXP exp
#define DSQRT sqrt

#define Too_Small_Time 2 /* Measurements should last at least 2 seconds */

#define HZ (1)


extern long time();

#define Start_Timer() Begin_Time = time ( (long *) 0)
#define Stop_Timer()  End_Time   = time ( (long *) 0)
#define Mic_secs_Per_Second     1000000.0

double T,T1,T2,E1[5];
int NJ,NK,L;

main (argc, argv) int argc; char *argv[];
{
	/* used in the FORTRAN version */
	long I;
	long N1, N2, N3, N4, N6, N7, N8, N9, N10, N11;
	double X1,X2,X3,X4,X,Y,Z;
	long LOOP;
	int II, NJNJ;

	/* added for this version */
	long loopstart;
	long startsec, finisec;
	float NKIPS;
	int continuous;

long            Begin_Time, End_Time, User_Time;
float           Microseconds, W_Per_Second;

	loopstart = 1000;		/* see the note about LOOP below */
	continuous = 0;

	II = 1;		/* start at the first arg (temp use of II here) */
	while (II < argc) {
		if (strncmp(argv[II], "-c", 2) == 0 || argv[II][0] == 'c') {
			continuous = 1;
		} else if (atol(argv[II]) > 0) {
			loopstart = atol(argv[II]);
		} else {
			fprintf(stderr,  "usage: whetdc [-c] [loops]\n");
			return(1);
		}
		II++;
	}

LCONT:
    Start_Timer();
	T  = .499975;
	T1 = 0.50025;
	T2 = 2.0;
	LOOP = loopstart;
	II   = 1;

	NJNJ = 1;

IILOOP:
	N1  = 0;
	N2  = 12 * LOOP;
	N3  = 14 * LOOP;
	N4  = 345 * LOOP;
	N6  = 210 * LOOP;
	N7  = 32 * LOOP;
	N8  = 899 * LOOP;
	N9  = 616 * LOOP;
	N10 = 0;
	N11 = 93 * LOOP;
/*
C
C	Module 1: Simple identifiers
C
*/
	X1  =  1.0;
	X2  = -1.0;
	X3  = -1.0;
	X4  = -1.0;

	for (I = 1; I <= N1; I++) {
	    X1 = (X1 + X2 + X3 - X4) * T;
	    X2 = (X1 + X2 - X3 + X4) * T;
	    X3 = (X1 - X2 + X3 + X4) * T;
	    X4 = (-X1+ X2 + X3 + X4) * T;
	}

	if (NJNJ==II)POUT(N1,N1,N1,X1,X2,X3,X4);


/*
C
C	Module 2: Array elements
C
*/
	E1[1] =  1.0;
	E1[2] = -1.0;
	E1[3] = -1.0;
	E1[4] = -1.0;

	for (I = 1; I <= N2; I++) {
	    E1[1] = ( E1[1] + E1[2] + E1[3] - E1[4]) * T;
	    E1[2] = ( E1[1] + E1[2] - E1[3] + E1[4]) * T;
	    E1[3] = ( E1[1] - E1[2] + E1[3] + E1[4]) * T;
	    E1[4] = (-E1[1] + E1[2] + E1[3] + E1[4]) * T;
	}


	if (NJNJ==II)POUT(N2,N3,N2,E1[1],E1[2],E1[3],E1[4]);


/*
C
C	Module 3: Array as parameter
C
*/
for (I = 1; I <= N3; I++) PA(E1);
	if (NJNJ==II)POUT(N3,N2,N2,E1[1],E1[2],E1[3],E1[4]);
	NJ = 1;
	for (I = 1; I <= N4; I++) {
		if (NJ == 1)
			NJ = 2;
		else
			NJ = 3;

		if (NJ > 2)
			NJ = 0;
		else
			NJ = 1;

		if (NJ < 1)
			NJ = 1;
		else
			NJ = 0;
	}


	if (NJNJ==II)POUT(N4,NJ,NJ,X1,X2,X3,X4);


/*
C
C	Module 5: Omitted
C 	Module 6: Integer arithmetic
C
*/

	NJ = 1;
	NK = 2;
	L = 3;

	for (I = 1; I <= N6; I++) {
	    NJ = NJ * (NK-NJ) * (L-NK);
	    NK = L * NK - (L-NJ) * NK;
	    L = (L-NK) * (NK+NJ);
	    E1[L-1] = NJ + NK + L;
	    E1[NK-1] = NJ * NK * L;
	}


	if (NJNJ==II)POUT(N6,NJ,NK,E1[1],E1[2],E1[3],E1[4]);


/*
C
C	Module 7: Trigonometric functions
C
*/
	X = 0.5;
	Y = 0.5;

	for (I = 1; I <= N7; I++) {
		X = T * DATAN(T2*DSIN(X)*DCOS(X)/(DCOS(X+Y)+DCOS(X-Y)-1.0));
		Y = T * DATAN(T2*DSIN(Y)*DCOS(Y)/(DCOS(X+Y)+DCOS(X-Y)-1.0));
	}


	if (NJNJ==II)POUT(N7,NJ,NK,X,X,Y,Y);


/*
C
C	Module 8: Procedure calls
C
*/
	X = 1.0;
	Y = 1.0;
	Z = 1.0;

	for (I = 1; I <= N8; I++)
		P3(X,Y,&Z);


	if (NJNJ==II)POUT(N8,NJ,NK,X,Y,Z,Z);


/*
C
C	Module 9: Array references
C
*/
	NJ = 1;
	NK = 2;
	L = 3;
	E1[1] = 1.0;
	E1[2] = 2.0;
	E1[3] = 3.0;

	for (I = 1; I <= N9; I++)
		P0();


	if (NJNJ==II)POUT(N9,NJ,NK,E1[1],E1[2],E1[3],E1[4]);


/*
C
C	Module 10: Integer arithmetic
C
*/
	NJ = 2;
	NK = 3;

	for (I = 1; I <= N10; I++) {
	    NJ = NJ + NK;
	    NK = NJ + NK;
	    NJ = NK - NJ;
	    NK = NK - NJ - NJ;
	}


	if (NJNJ==II)POUT(N10,NJ,NK,X1,X2,X3,X4);


/*
C
C	Module 11: Standard functions
C
*/
	X = 0.75;

	for (I = 1; I <= N11; I++)
		X = DSQRT(DEXP(DLOG(X)/T1));


	if (NJNJ==II)POUT(N11,NJ,NK,X,X,X,X);


/*
C
C      THIS IS THE END OF THE MANJOR LOOP.
C
*/
	if (++NJNJ <= II)
		goto IILOOP;

/*
C
C      Stop benchmark timing at this point.
C
*/
Stop_Timer();
User_Time = End_Time - Begin_Time;
/*
C----------------------------------------------------------------
C      Performance in Whetstone NKIP's per second is given by
C
C	(100*LOOP*II)/TIME
C
C      where TIME is in seconds.
C--------------------------------------------------------------------
*/
	printf("\n");

	printf("Loops: %ld, Iterations: %d, Duration: %ld sec.\n",
			LOOP, II, User_Time);

	NKIPS = (100.0*LOOP*II)/(float)(User_Time);
	if (NKIPS >= 1000.0)
		printf("C Converted Double Precision Whetstones: %.1f MIPS\n", NKIPS/1000.0);
	else
		printf("C Converted Double Precision Whetstones: %.1f NKIPS\n", NKIPS);
	
	Microseconds = (float) User_Time * Mic_secs_Per_Second / ((float) HZ * ((float) LOOP));
	W_Per_Second = ((float) HZ * (float) LOOP) / (float) User_Time;
	printf ("Microseconds for one run through Whetstone");
	printf ("%10.1f \n", Microseconds);
	printf ("Whetstones per Second:                      ");
	printf ("%10.0f \n", W_Per_Second);
	printf ("\n");

	if (continuous)
		goto LCONT;
	return(0);
}

PA(E) double E[];
{
	NJ = 0;

L10:
	E[1] = ( E[1] + E[2] + E[3] - E[4]) * T;
	E[2] = ( E[1] + E[2] - E[3] + E[4]) * T;
	E[3] = ( E[1] - E[2] + E[3] + E[4]) * T;
	E[4] = (-E[1] + E[2] + E[3] + E[4]) / T2;
	NJ += 1;

	if (NJ < 6)
		goto L10;
}

P0()
{
	E1[NJ] = E1[NK];
	E1[NK] = E1[L];
	E1[L] = E1[NJ];
}

P3(X, Y, Z)
	double X; 
	double Y;
	double *Z; {
	double X1, Y1; 

	X1 = X;
	Y1 = Y;
	X1 = T * (X1 + Y1);
	Y1 = T * (X1 + Y1);
	*Z  = (X1 + Y1) / T2;
}


POUT(N, NJ, NK, X1, X2, X3, X4)
	long N;
	long NJ;
	long NK;
	double X1;
	double X2;
	double X3;
	double X4; {
	printf("%7ld %7ld %7ld %12.4e %12.4e %12.4e %12.4e\n",
						N, NJ, NK, X1, X2, X3, X4);
}

